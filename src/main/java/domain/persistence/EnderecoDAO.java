package domain.persistence;

import javax.persistence.EntityManager;

import domain.model.Endereco;

public class EnderecoDAO extends AbstractDAO<Endereco>{

	public EnderecoDAO(EntityManager entityManager) {
		super(entityManager, Endereco.class);
	}

}
