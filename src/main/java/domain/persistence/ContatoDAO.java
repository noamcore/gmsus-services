package domain.persistence;

import javax.persistence.EntityManager;

import domain.model.Contato;

public class ContatoDAO extends AbstractDAO<Contato>{

	public ContatoDAO(EntityManager entityManager) {
		super(entityManager, Contato.class);
	}

}
