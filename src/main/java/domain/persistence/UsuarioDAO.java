package domain.persistence;

import java.util.List;

import javax.persistence.EntityManager;

import domain.model.Usuario;

public class UsuarioDAO extends AbstractDAO<Usuario> {

	public UsuarioDAO(EntityManager entityManager) {
		super(entityManager, Usuario.class);
	}

	public List<Usuario> getAll() {
		return getEntityManager().createQuery("select u from Usuario u", Usuario.class).getResultList();
	}

	public List<Usuario> getAllByName(String nome) {
		return getEntityManager().createQuery("select U from usuario U where U.nome like :nome", Usuario.class)
				.setParameter("nome", "%" + nome + "%").getResultList();
	}
}
