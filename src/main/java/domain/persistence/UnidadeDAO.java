package domain.persistence;

import javax.persistence.EntityManager;

import domain.model.Unidade;

public class UnidadeDAO extends AbstractDAO<Unidade> {

	public UnidadeDAO(EntityManager entityManager) {
		super(entityManager, Unidade.class);
	}

}
