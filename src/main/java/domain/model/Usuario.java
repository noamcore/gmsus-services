package domain.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Usuario {

	@Id
	@GeneratedValue
	private Long id;

	@Column
	private String nome;

	@Column
	private String cpf;

	@Column
	private String numeroSus;

	@Column
	private LocalDateTime dataNasc;

	@OneToOne(cascade = CascadeType.PERSIST)
	private Contato contato;

	@OneToOne(cascade = CascadeType.PERSIST)
	private Endereco endereco;

	@OneToMany(fetch = FetchType.LAZY)
	private List<Prescricao> prescricoes;

	public Usuario() {
		// TODO Auto-generated constructor stub
	}
	
	public Usuario(String nome, String cpf, String numeroSus, LocalDateTime dataNasc, Contato contato,
			Endereco endereco) {
		super();
		this.nome = nome;
		this.cpf = cpf;
		this.numeroSus = numeroSus;
		this.dataNasc = dataNasc;
		this.contato = contato;
		this.endereco = endereco;
		prescricoes = new ArrayList<>();
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNumeroSus() {
		return numeroSus;
	}

	public void setNumeroSus(String numeroSus) {
		this.numeroSus = numeroSus;
	}

	public LocalDateTime getDataNasc() {
		return dataNasc;
	}

	public void setDataNasc(LocalDateTime dataNasc) {
		this.dataNasc = dataNasc;
	}

	public Contato getContato() {
		return contato;
	}

	public void setContato(Contato contato) {
		this.contato = contato;
	}

	public Long getId() {
		return id;
	}

	public List<Prescricao> getPrescricoes() {
		return prescricoes;
	}
	
	public void setPrescricoes(List<Prescricao> prescricoes) {
		this.prescricoes = prescricoes;
	}
	
	public Endereco getEndereco() {
		return endereco;
	}
	
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
}
